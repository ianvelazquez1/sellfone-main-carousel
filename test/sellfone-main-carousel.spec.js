/* eslint-disable no-unused-expressions */
import { fixture, assert } from "@open-wc/testing";

import "../sellfone-main-carousel.js";

describe("Suite cases", () => {
  it("Case default", async () => {
    const _element = await fixture("<sellfone-main-carousel></sellfone-main-carousel>");
    assert.strictEqual(_element.hello, 'Hello World!');
  });
});
