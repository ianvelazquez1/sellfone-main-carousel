import { html, LitElement } from 'lit-element';
import style from './sellfone-main-carousel-styles.js';

class SellfoneMainCarousel extends LitElement {
  static get properties() {
    return {
        imagesArray:{
        type:Array,
        attribute:'images-array'
        },
        __slideIndex:{
          type:Number
        }
    };
  }

  static get styles() {
    return style;
  }

  constructor() {
    super();
    this.imagesArray=[];
    this.__slideIndex=0;
  }

  render() {
    return html`
      <div class="sliderContainer">
        ${this.imagesArray.map(image=>{
          return html`
          <div class="mySlide inactive fade">
            <img class="sliderImage" src="${image.src}" alt="${image.src}">
          </div>
          `
        })}
        <a class="prev" @click="${(e)=>this.nextSlide(-1)}">&#10094;</a>
        <a class="next" @click="${(e)=>this.nextSlide(1)}">&#10095;</a>
      <div>
      `;
    }

    firstUpdated(){
      this.showSlide(this.__slideIndex);
    }

    nextSlide(index){
      this.__slideIndex+=index;
      this.showSlide(this.__slideIndex);
    }

    showSlide(index){
      const slides=this.shadowRoot.querySelectorAll(".mySlide");
      for(const slide of slides){
        slide.classList.remove("active");
      }
      if(index+1>slides.length){
        slides[0].classList.add("active");
        this.__slideIndex=0;
      }else if(index<0){
        slides[0].classList.add("active");
        this.__slideIndex=0;
      }
      slides[index].classList.add("active");
     
    }



}



window.customElements.define("sellfone-main-carousel", SellfoneMainCarousel);
